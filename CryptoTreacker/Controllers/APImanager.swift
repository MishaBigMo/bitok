//
//  APImanager.swift
//  CryptoTreacker
//
//  Created by Михаил on 09/07/2019.
//  Copyright © 2019 Михаил. All rights reserved.
//

import Foundation
import Alamofire

class ApiManager {
    // pattern - singleTorn
    static var instance = ApiManager()
    
    let headers: HTTPHeaders = [
        "X-CoinAPI-Key": "C159FA6D-C95B-411F-8174-55F761479803",
        "Accept": "application/json"
    ]
    
    private enum Constants {
        static let baseURL = "https://pro-api.coinmarketcap.com/v1/cryptocurrency/listings/latest"
        static let baseURL2 = "https://rest.coinapi.io"

    }
    
    private enum EndPoints {
        static let coin2 = "/v1/exchanges"
        static let BTC = "/v1/exchangerate/BTC/USD"
        static let ETH = "/v1/exchangerate/ETH/USD"
        static let LTC = "/v1/exchangerate/LTC/USD"
        static let DASH = "/v1/exchangerate/DASH/USD"
        static let XLM = "/v1/exchangerate/XLM/USD"
        static let ADA = "/v1/exchangerate/ADA/USD"
        static let XRP = "/v1/exchangerate/ADA/USD"
        static let NEO = "/v1/exchangerate/NEO/USD"
        static let ZEC = "/v1/exchangerate/ZEC/USD"
        static let XMR = "/v1/exchangerate/XMR/USD"
        static let test = "/v1/symbols?filter_symbol_id={BITSTAMP_SPOT_BTC_USD}"


        
    }
    
    
    func getXRPData(onComlete: @escaping (Crypta) -> Void) {
        AF.request(Constants.baseURL2 + EndPoints.XRP, method: .get, parameters: [:], headers : headers).responseJSON { (respons) in
            print(respons.result)
            switch respons.result {
                
            case .success(let data):
                if let datas = data as? Dictionary<String, Any> {
                    let crypta = Crypta()
                    crypta.asset_id_base = datas["asset_id_base"] as? String ?? "no data"
                    crypta.asset_id_quote = datas["asset_id_quote"] as? String ?? "no data"
                    crypta.time = datas["time"] as? String ?? "no data"
                    crypta.rate = datas["rate"] as? Double ?? 0.0
                    onComlete(crypta)
                    
                } else {
                    print("else")
                }
            case .failure(let error):
                print(error)
            }
        }
    }
    
    func getADAData(onComlete: @escaping (Crypta) -> Void) {
        AF.request(Constants.baseURL2 + EndPoints.ADA, method: .get, parameters: [:], headers : headers).responseJSON { (respons) in
            print(respons.result)
            switch respons.result {
                
            case .success(let data):
                if let datas = data as? Dictionary<String, Any> {
                    let crypta = Crypta()
                    crypta.asset_id_base = datas["asset_id_base"] as? String ?? "no data"
                    crypta.asset_id_quote = datas["asset_id_quote"] as? String ?? "no data"
                    crypta.time = datas["time"] as? String ?? "no data"
                    crypta.rate = datas["rate"] as? Double ?? 0.0
                    onComlete(crypta)
                    
                } else {
                    print("else")
                }
            case .failure(let error):
                print(error)
            }
        }
    }

    func getXLMData(onComlete: @escaping (Crypta) -> Void) {
        AF.request(Constants.baseURL2 + EndPoints.XLM, method: .get, parameters: [:], headers : headers).responseJSON { (respons) in
            print(respons.result)
            switch respons.result {
                
            case .success(let data):
                if let datas = data as? Dictionary<String, Any> {
                    let crypta = Crypta()
                    crypta.asset_id_base = datas["asset_id_base"] as? String ?? "no data"
                    crypta.asset_id_quote = datas["asset_id_quote"] as? String ?? "no data"
                    crypta.time = datas["time"] as? String ?? "no data"
                    crypta.rate = datas["rate"] as? Double ?? 0.0
                    onComlete(crypta)
                    
                } else {
                    print("else")
                }
            case .failure(let error):
                print(error)
            }
        }
    }
    
    func getXMRData(onComlete: @escaping (Crypta) -> Void) {
        AF.request(Constants.baseURL2 + EndPoints.XMR, method: .get, parameters: [:], headers : headers).responseJSON { (respons) in
            print(respons.result)
            switch respons.result {
                
            case .success(let data):
                if let datas = data as? Dictionary<String, Any> {
                    let crypta = Crypta()
                    crypta.asset_id_base = datas["asset_id_base"] as? String ?? "no data"
                    crypta.asset_id_quote = datas["asset_id_quote"] as? String ?? "no data"
                    crypta.time = datas["time"] as? String ?? "no data"
                    crypta.rate = datas["rate"] as? Double ?? 0.0
                    onComlete(crypta)
                    
                } else {
                    print("else")
                }
            case .failure(let error):
                print(error)
            }
        }
    }

    func getZECData(onComlete: @escaping (Crypta) -> Void) {
        AF.request(Constants.baseURL2 + EndPoints.ZEC, method: .get, parameters: [:], headers : headers).responseJSON { (respons) in
            print(respons.result)
            switch respons.result {
                
            case .success(let data):
                if let datas = data as? Dictionary<String, Any> {
                    let crypta = Crypta()
                    crypta.asset_id_base = datas["asset_id_base"] as? String ?? "no data"
                    crypta.asset_id_quote = datas["asset_id_quote"] as? String ?? "no data"
                    crypta.time = datas["time"] as? String ?? "no data"
                    crypta.rate = datas["rate"] as? Double ?? 0.0
                    onComlete(crypta)
                    
                } else {
                    print("else")
                }
            case .failure(let error):
                print(error)
            }
        }
    }

    func getNEOData(onComlete: @escaping (Crypta) -> Void) {
        AF.request(Constants.baseURL2 + EndPoints.NEO, method: .get, parameters: [:], headers : headers).responseJSON { (respons) in
            print(respons.result)
            switch respons.result {
                
            case .success(let data):
                if let datas = data as? Dictionary<String, Any> {
                    let crypta = Crypta()
                    crypta.asset_id_base = datas["asset_id_base"] as? String ?? "no data"
                    crypta.asset_id_quote = datas["asset_id_quote"] as? String ?? "no data"
                    crypta.time = datas["time"] as? String ?? "no data"
                    crypta.rate = datas["rate"] as? Double ?? 0.0
                    onComlete(crypta)
                    
                } else {
                    print("else")
                }
            case .failure(let error):
                print(error)
            }
        }
    }
    
    func getDASHData(onComlete: @escaping (Crypta) -> Void) {
        AF.request(Constants.baseURL2 + EndPoints.DASH, method: .get, parameters: [:], headers : headers).responseJSON { (respons) in
            print(respons.result)
            switch respons.result {
                
            case .success(let data):
                if let datas = data as? Dictionary<String, Any> {
                    let crypta = Crypta()
                    crypta.asset_id_base = datas["asset_id_base"] as? String ?? "no data"
                    crypta.asset_id_quote = datas["asset_id_quote"] as? String ?? "no data"
                    crypta.time = datas["time"] as? String ?? "no data"
                    crypta.rate = datas["rate"] as? Double ?? 0.0
                    onComlete(crypta)
                    
                } else {
                    print("else")
                }
            case .failure(let error):
                print(error)
            }
        }
    }
    
    func getLightCoinData(onComlete: @escaping (Crypta) -> Void) {
        AF.request(Constants.baseURL2 + EndPoints.LTC, method: .get, parameters: [:], headers : headers).responseJSON { (respons) in
            print(respons.result)
            switch respons.result {
                
            case .success(let data):
                if let datas = data as? Dictionary<String, Any> {
                    let crypta = Crypta()
                    crypta.asset_id_base = datas["asset_id_base"] as? String ?? "no data"
                    crypta.asset_id_quote = datas["asset_id_quote"] as? String ?? "no data"
                    crypta.time = datas["time"] as? String ?? "no data"
                    crypta.rate = datas["rate"] as? Double ?? 0.0
                    onComlete(crypta)
                    
                } else {
                    print("else")
                }
            case .failure(let error):
                print(error)
            }
        }
    }
    
    func getEtheriumData(onComlete: @escaping (Crypta) -> Void) {
        AF.request(Constants.baseURL2 + EndPoints.ETH, method: .get, parameters: [:], headers : headers).responseJSON { (respons) in
            print(respons.result)
            switch respons.result {
                
            case .success(let data):
                if let datas = data as? Dictionary<String, Any> {
                    let crypta = Crypta()
                    crypta.asset_id_base = datas["asset_id_base"] as? String ?? "no data"
                    crypta.asset_id_quote = datas["asset_id_quote"] as? String ?? "no data"
                    crypta.time = datas["time"] as? String ?? "no data"
                    crypta.rate = datas["rate"] as? Double ?? 0.0
                    onComlete(crypta)
                    
                } else {
                    print("else")
                }
            case .failure(let error):
                print(error)
            }
        }
    }

    func getBitCoinData(onComlete: @escaping (Crypta) -> Void) {
        AF.request(Constants.baseURL2 + EndPoints.BTC, method: .get, parameters: [:], headers : headers).responseJSON { (respons) in
            print(respons.result)
            switch respons.result {
                
            case .success(let data):
                if let datas = data as? Dictionary<String, Any> {
                    let crypta = Crypta()
                    crypta.asset_id_base = datas["asset_id_base"] as? String ?? "no data"
                    crypta.asset_id_quote = datas["asset_id_quote"] as? String ?? "no data"
                    crypta.time = datas["time"] as? String ?? "no data"
                    crypta.rate = datas["rate"] as? Double ?? 0.0
                    onComlete(crypta)
                    
                } else {
                    print("else")
                }
            case .failure(let error):
                print(error)
            }
        }
    }
    
}




