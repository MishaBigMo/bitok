//
//  ViewController.swift
//  CryptoTreacker
//
//  Created by Михаил on 09/07/2019.
//  Copyright © 2019 Михаил. All rights reserved.
//
import Firebase
import Alamofire
import UIKit

class ViewController: UIViewController {
    // MARK: - Properties
    var textButtonCell: String?
    var crypts: [Crypta] = []
    
    
    // MARK: - Outlets
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var tableView: UITableView!
    
    
    // MARK: - Actions
    @IBAction func purchaseTapped(_ sender: Any) {
        print("purchase!")
    }
    
    
    
    // MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.allowsSelection = false

        // RemoteConfig Firebase
        let remoteConfig = RemoteConfig.remoteConfig()
        remoteConfig.fetch { (status, error) in
            remoteConfig.activateFetched()
            if let buttonTitle = remoteConfig["buttonTittle"].stringValue {
                self.textButtonCell = "\(buttonTitle)"
                self.tableView.reloadData()
            }
        }

        // get and set current date
        let currentDateTime = Date()
        let formatter = DateFormatter()
        formatter.timeStyle = .short
        formatter.dateStyle = .medium
        formatter.string(from: currentDateTime)
        dateLabel.text = "Update on \(currentDateTime)"
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        // get data from server
        ApiManager.instance.getXRPData { (xpr) in
            let cryp = Crypta()
            cryp.asset_id_base = xpr.asset_id_base
            cryp.asset_id_quote = xpr.asset_id_quote
            cryp.rate = xpr.rate
            cryp.time = xpr.time
            print(cryp.asset_id_quote)
            print(cryp.asset_id_base)
            print(cryp.rate)
            print(cryp.time)
            self.crypts.append(cryp)
            
            if self.crypts.count > 9 {
                self.tableView.reloadData()
            }
            
        }
        ApiManager.instance.getADAData { (ada) in
            let cryp = Crypta()
            cryp.asset_id_base = ada.asset_id_base
            cryp.asset_id_quote = ada.asset_id_quote
            cryp.rate = ada.rate
            cryp.time = ada.time
            print(cryp.asset_id_quote)
            print(cryp.asset_id_base)
            print(cryp.rate)
            print(cryp.time)
            self.crypts.append(cryp)
            
            if self.crypts.count > 9 {
                self.tableView.reloadData()
            }
            
        }
        ApiManager.instance.getXLMData { (xlm) in
            let cryp = Crypta()
            cryp.asset_id_base = xlm.asset_id_base
            cryp.asset_id_quote = xlm.asset_id_quote
            cryp.rate = xlm.rate
            cryp.time = xlm.time
            print(cryp.asset_id_quote)
            print(cryp.asset_id_base)
            print(cryp.rate)
            print(cryp.time)
            self.crypts.append(cryp)
            
            if self.crypts.count > 9 {
                self.tableView.reloadData()
            }
            
        }
        ApiManager.instance.getZECData { (zec) in
            let cryp = Crypta()
            cryp.asset_id_base = zec.asset_id_base
            cryp.asset_id_quote = zec.asset_id_quote
            cryp.rate = zec.rate
            cryp.time = zec.time
            print(cryp.asset_id_quote)
            print(cryp.asset_id_base)
            print(cryp.rate)
            print(cryp.time)
            self.crypts.append(cryp)
            
            if self.crypts.count > 9 {
                self.tableView.reloadData()
            }
            
        }
        ApiManager.instance.getNEOData { (neo) in
            let cryp = Crypta()
            cryp.asset_id_base = neo.asset_id_base
            cryp.asset_id_quote = neo.asset_id_quote
            cryp.rate = neo.rate
            cryp.time = neo.time
            print(cryp.asset_id_quote)
            print(cryp.asset_id_base)
            print(cryp.rate)
            print(cryp.time)
            self.crypts.append(cryp)
            
            if self.crypts.count > 9 {
                self.tableView.reloadData()
            }
            
        }
        ApiManager.instance.getDASHData { (dash) in
            let cryp = Crypta()
            cryp.asset_id_base = dash.asset_id_base
            cryp.asset_id_quote = dash.asset_id_quote
            cryp.rate = dash.rate
            cryp.time = dash.time
            print(cryp.asset_id_quote)
            print(cryp.asset_id_base)
            print(cryp.rate)
            print(cryp.time)
            self.crypts.append(cryp)
            
            if self.crypts.count > 9 {
                self.tableView.reloadData()
            }
            
        }
        ApiManager.instance.getEtheriumData { (etherium) in
            let cryp = Crypta()
            cryp.asset_id_base = etherium.asset_id_base
            cryp.asset_id_quote = etherium.asset_id_quote
            cryp.rate = etherium.rate
            cryp.time = etherium.time
            print(cryp.asset_id_quote)
            print(cryp.asset_id_base)
            print(cryp.rate)
            print(cryp.time)
            self.crypts.append(cryp)
            
            if self.crypts.count > 9 {
                self.tableView.reloadData()
            }
            
        }
        ApiManager.instance.getLightCoinData { (lightCoin) in
            let cryp = Crypta()
            cryp.asset_id_base = lightCoin.asset_id_base
            cryp.asset_id_quote = lightCoin.asset_id_quote
            cryp.rate = lightCoin.rate
            cryp.time = lightCoin.time
            print(cryp.asset_id_quote)
            print(cryp.asset_id_base)
            print(cryp.rate)
            print(cryp.time)
            self.crypts.append(cryp)
            
            if self.crypts.count > 9 {
                self.tableView.reloadData()
            }
            
        }
        ApiManager.instance.getBitCoinData { (a) in
            let cryp = Crypta()
            cryp.asset_id_base = a.asset_id_base
            cryp.asset_id_quote = a.asset_id_quote
            cryp.rate = a.rate
            cryp.time = a.time
            print(cryp.asset_id_quote)
            print(cryp.asset_id_base)
            print(cryp.rate)
            print(cryp.time)
            self.crypts.append(cryp)
            
            if self.crypts.count > 9 {
                self.tableView.reloadData()
            }
        }
        ApiManager.instance.getXMRData { (xmr) in
            let cryp = Crypta()
            cryp.asset_id_base = xmr.asset_id_base
            cryp.asset_id_quote = xmr.asset_id_quote
            cryp.rate = xmr.rate
            cryp.time = xmr.time
            print(cryp.asset_id_quote)
            print(cryp.asset_id_base)
            print(cryp.rate)
            print(cryp.time)
            self.crypts.append(cryp)
            
            if self.crypts.count > 9 {
                self.tableView.reloadData()
            }
            
        }
    }
}



extension ViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return crypts.count
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        cell.alpha = 0
        UIView.animate(
            withDuration: 0.5,
            delay: 0.05 * Double(indexPath.row),
            animations: {
                cell.alpha = 1
        })
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell1", for: indexPath) as! CryptoDataCell
       cell.button.setTitle(textButtonCell, for: .normal)
        
        let crypta = crypts[indexPath.row]
        switch crypta.asset_id_base {
        case "ADA":
            cell.name.text = "Cardano"
        case "XRP":
            cell.name.text = "Ripple"
        case "XLM":
            cell.name.text = "Stellar"
        case "NEO":
            cell.name.text = "NEO"
        case "ZEC":
            cell.name.text = "Zchain"
        case "ETH":
            cell.name.text = "Ethereum"
        case "LTC":
            cell.name.text = "Litecoin"
        case "DASH":
            cell.name.text = "Dash"
        case "XMR":
            cell.name.text = "Monero"
        case "BTC":
            cell.name.text = "Bitcoin"
        default:
            break
        }

        cell.shortName.text = crypta.asset_id_base
        if let data = crypta.rate {
            cell.price.text = "Price: $\(Double(round(1000*data)/1000))"
        }


        switch crypta.asset_id_base {
        case "XLM":
            cell.cryptoImage?.image = UIImage(named: "XLM")
        case "ADA":
            cell.cryptoImage?.image = UIImage(named: "ADA")
        case "XRP":
            cell.cryptoImage?.image = UIImage(named: "XRP")
        case "NEO":
            cell.cryptoImage?.image = UIImage(named: "NEO")
        case "BTC":
            cell.cryptoImage?.image = UIImage(named: "Bitcoin")
        case "ETH":
            cell.cryptoImage?.image = UIImage(named: "Ethereum")
        case "LTC":
            cell.cryptoImage?.image = UIImage(named: "Litecoin")
        case "DASH":
            cell.cryptoImage?.image = UIImage(named: "Dash")
        case "ZEC":
            cell.cryptoImage?.image = UIImage(named: "Zec")
        case "XMR":
            cell.cryptoImage?.image = UIImage(named: "Monero")
        default:
            break
        }
        
        return cell
    }
    
    
}


