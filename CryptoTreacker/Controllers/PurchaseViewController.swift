//
//  PurchaseViewController.swift
//  CryptoTreacker
//
//  Created by Михаил on 12/07/2019.
//  Copyright © 2019 Михаил. All rights reserved.
//
import Firebase
import UIKit

class PurchaseViewController: UIViewController {
    // MARK: - Properties
    
    
    
    // MARK: - Outlets
    @IBOutlet weak var button: RoundButton!
    @IBOutlet weak var label: UILabel!
    
    
    
    // MARK: - Actions
    @IBAction func BuyTapped(_ sender: Any) {
        dismiss(animated: true, completion: nil)
        print("Покупка удалась")
    }
    
    
    
    // MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        label.alpha = 0
        button.alpha = 0
        Analytics.setScreenName("PurchaseViewController", screenClass: "PurchaseViewController")
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        UIView.animate(withDuration: 1, animations: {
            self.label.alpha = 0
        }) { (true) in
            self.showButton()
        }
    }
    
    
    
    
    func showButton() {
        UIView.animate(withDuration: 1, animations: {
            self.button.alpha = 1
            self.label.alpha = 1
        })
    }
    
}
