//
//  CryptoDataCell.swift
//  CryptoTreacker
//
//  Created by Михаил on 09/07/2019.
//  Copyright © 2019 Михаил. All rights reserved.
//

import UIKit

class CryptoDataCell: UITableViewCell {
    
    @IBOutlet weak var button: UIButton!
    @IBOutlet weak var shortName: UILabel!
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var price: UILabel!
    @IBOutlet weak var cryptoImage: UIImageView!
    
    
}
